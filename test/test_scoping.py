import unittest
from slimit import parser, minify
from scramblit.scope.visitor import ScopeVisitor
from scramblit.scope.namegenerators import PrefixedNumberNameGenerator

class TestScoping(unittest.TestCase):
    def test_that_we_properly_generate_variable_identifiers(self):
        code = '''
var a = 0;
function func_name(argument_1) {
  var b = 0;
  return b+argument_1;
}
        '''
        expected = '''
var v0 = 0;
function f1(a2) {
  var v3 = 0;
  return v3 + a2;
}
'''
        p = parser.Parser(lex_optimize=False, yacc_optimize=False)
        tree = p.parse(code)
        vis = ScopeVisitor()
        vis.scope.set_external_symbols()
        vis.scope.name_generator = PrefixedNumberNameGenerator()
        vis.scope.forced_mangled=True
        fixtree = vis.visit(tree)
        vis.scope.mangle()
        result = fixtree.to_ecma()
        #print(result)
        self.assertEqual(expected.strip(), result.strip())
    def test_that_browser_variables_are_preserved(self):
        code = '''
var a = window;
function func_name(argument_1) {
  var b = 0;
  return a+b+argument_1;
}
        '''
        expected = '''
var v0 = window;
function f1(a2) {
  var v3 = 0;
  return v0 + v3 + a2;
}
'''
        p = parser.Parser(lex_optimize=False, yacc_optimize=False)
        tree = p.parse(code)
        vis = ScopeVisitor()
        vis.scope.set_external_symbols()
        vis.scope.name_generator = PrefixedNumberNameGenerator()
        vis.scope.forced_mangled=True
        fixtree = vis.visit(tree)
        vis.scope.mangle()
        result = fixtree.to_ecma()
        #print(result)
        self.assertEqual(expected.strip(), result.strip())
    def test_that_dot_accessors_are_not_mangled(self):
        code = '''
var a = window;
function func_name(argument_1) {
  var b = 0;
  return a.setTimer+b+argument_1;
}
        '''
        expected = '''
var v0 = window;
function f1(a2) {
  var v3 = 0;
  return v0.setTimer + v3 + a2;
}
'''
        p = parser.Parser(lex_optimize=False, yacc_optimize=False)
        tree = p.parse(code)
        vis = ScopeVisitor()
        vis.scope.set_external_symbols()
        vis.scope.name_generator = PrefixedNumberNameGenerator()
        vis.scope.forced_mangled=True
        fixtree = vis.visit(tree)
        vis.scope.mangle()
        result = fixtree.to_ecma()
        #print(result)
        self.assertEqual(expected.strip(), result.strip())

if __name__ == '__main__':
    unittest.main()
