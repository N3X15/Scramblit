import unittest
from slimit import parser, minify
from scramblit.scope.visitor import ScopeVisitor
from scramblit.scope.namegenerators import SequentialASCIINameGenerator

class TestSequentialASCIINameGenerator(unittest.TestCase):
    def setUp(self):
        self.namegen = SequentialASCIINameGenerator()

    def test_that_it_generates_valid_ids(self):
        tests = {
            0: 'a', # 97+0
            1: 'b', # 97+1
           20: 'u', # 97+20
           51: 'Z',
           52: 'ba',
        }
        for argument,expected in tests.items():
            result = self.namegen.gen_name_suffix(argument)
            #print(argument,result,expected)
            self.assertEqual(result,expected)

if __name__ == '__main__':
    unittest.main()
