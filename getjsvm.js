window.onload = (function() {
  function isFunction(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]'
  };
  var o = '';
  o += '# ' + window.navigator.userAgent + '\n';
  var v = Object.getOwnPropertyNames(window);
  v.sort();
  for (var i = 0; i < v.length; i++) {
    var k = v[i];
    if (['isFunction', 'o', 'k'].indexOf(k) === -1) {
      o += 'self.define_external("' + k + '")\n';
    }
  }
  document.getElementById('a').value = o;
});
