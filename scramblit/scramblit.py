'''
Scramblit Core

Copyright (c) 2017 - 2019 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''

import random
from typing import List

from scramblit.scope.scope import Scope
#from scramblit.visitors import ReplacementVisitor
from scramblit.scope.visitor import ScopeVisitor
from slimit import ast, mangler
from slimit.minifier import ECMAMinifier
from slimit.parser import Parser


class Scramblit(ScopeVisitor):
    '''
    This class does all the heavy lifting of scrambling strings.

    Also
    '''

    def __init__(self, scrambler):
        super().__init__()

        self.double_mangle = True
        self.readable = False

        self.alphabet = 'abcdefghijlkmnopqrstuvwxyz'
        self.alphabet += self.alphabet.upper()
        self.alphabet += '_$'

        self.scope = Scope()

        self.mangler = scrambler(self)
        self.string_table_id = self.mangler.scope.assign_variable('_')
        self.zip_func_id = self.mangler.scope.assign_function('z',[])

    def fixObjectLiteral(self, obj):
        args = []
        for prop in obj.properties:
            args += [ast.String(repr(prop.left.value)), prop.right]
        return ast.FunctionCall(identifier=self.zip_func_id.node, args=args)

    def visit(self, node, attr=None, parent=None):
        if isinstance(node, ast.Object):
            node = self.fixObjectLiteral(node)
        super().visit(node, attr, parent)

        if isinstance(node, ast.String):
            return self.mangler.addStringToTable(node.value.strip("'\""))
        elif isinstance(node, ast.DotAccessor) and isinstance(node.identifier, ast.Identifier):
            o = self.mangler.addStringToTable(node.identifier.value)
            return ast.BracketAccessor(node.node, o)

        return node

    def build_zip(self) -> List[ast.Node]:
        '''
        var a=arguments;
        var o={};
        for(var i=0;i<a.length;i+=2) {
            o[a[i]]=a[i+1];
        }
        return o;
        '''
        scope = self.mangler.scope.build_scope()
        id_a = scope.assign_variable('a')
        id_o = scope.assign_variable('o')
        id_i = scope.assign_variable('i')
        return ast.FuncDecl(
            identifier=self.zip_func_id.node,
            parameters=[id_a.node],
            elements=[
                ast.VarStatement([
                    ast.VarDecl(id_a.node, ast.Identifier('arguments')),
                    ast.VarDecl(id_o.node, ast.Object([])),
                ]),
                ast.For(
                    init=ast.VarStatement(children=[ast.VarDecl(id_i.node, ast.Number('0'))]),
                    cond=ast.BinOp(
                        op='<',
                        left=id_i.node,
                        right=ast.DotAccessor(
                            node=id_a.node,
                            identifier=ast.Identifier('length')
                        )
                    ),
                    count=ast.Assign(
                        op='+=',
                        left=id_i.node,
                        right=ast.Number('2')
                    ),
                    statement=ast.Block(
                        children=[ast.ExprStatement(
                            ast.Assign(
                                op='=',
                                left=ast.BracketAccessor(
                                    node=id_o.node,
                                    expr=ast.BracketAccessor(
                                        node=id_a.node,
                                        expr=id_i.node
                                    )
                                ),
                                right=ast.BracketAccessor(
                                    node=id_a.node,
                                    expr=ast.BinOp(
                                        op='+',
                                        left=id_i.node,
                                        right=ast.Number('1')
                                    )
                                )
                            )
                        )
                    ])
                )
            ])

    def process_tree(self, tree, mangle_strings=False, bare=False, mangle=True, mangle_toplevel=False, double_mangle=True):
        tree = self.visit(tree)
        children = None
        if isinstance(tree, ast.Program):
            children = tree._children_list
        elif isinstance(tree, (ast.FuncBase,)):
            children = tree.elements
        if mangle_strings:
            tree = ast.Program(self.mangler.build_runtime())
            tree._children_list += [self.build_zip()]
            #if self.double_mangle:
            #    sf = Scramblit(self.mangler.__class__)
            #    tree = sf.process_tree(tree, mangle_strings=False, bare=True, mangle=False, mangle_toplevel=False)
            tree._children_list += [ast.VarStatement([ast.VarDecl(self.string_table_id.node, self.postProcess(mangle_strings))])]
            children = tree._children_list + children
        if not bare:
            # IIFE
            func = ast.FuncExpr(None, [], children)
            func._parens = True  # Workaround for stupid shit.
            children = [ast.ExprStatement(ast.FunctionCall(func, []))]
        tree = ast.Program(children)
        if mangle:
            mangler.mangle(tree, toplevel=mangle_toplevel)
        return tree

    def postProcess(self, mangle_strings):
        finished_string_table = self.mangler.getTable()

        o = []
        for x in finished_string_table:
            if mangle_strings:
                o += [self.mangler.process_string(x)]
            else:
                o += [ast.String(repr(x))]
        return ast.Array(o)

    def process_text(self, string, mangle_strings=False, bare=False, mangle=True, mangle_toplevel=False, minify=True, double_mangle=True):
        tree = Parser().parse(string)
        tree = self.process_tree(tree, mangle_strings, bare, mangle, mangle_toplevel, double_mangle)
        if minify:
            return ECMAMinifier().visit(tree)
        else:
            return tree.to_ecma()
