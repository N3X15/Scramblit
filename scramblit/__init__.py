'''
Scramblit commandline stuff.

Copyright (c) 2017 - 2019 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
from scramblit.scramblit import Scramblit
from scramblit.scramblers import NullScrambler, ArgonLikeScrambler, FatScrambler
from scramblit.scope.namegenerators import PrefixedNumberNameGenerator

__all__=['Scramblit','NullScrambler','ArgonLikeScrambler','FatScrambler']
AVAILABLE_SCRAMBLERS = {
    'fat': FatScrambler,
    'null': NullScrambler,
    'argonlike': ArgonLikeScrambler,
}

def main():
    import argparse
    argp = argparse.ArgumentParser()
    argp.add_argument('-t', '--mangle-strings', action='store_true', help='Fucks with strings.  Significantly adds to the size of the output.')
    argp.add_argument('-m', '--mangle', action='store_true', help='Mangles identifiers.')
    argp.add_argument('-B', '--bare', action='store_true', help='Does not wrap everything in an anonymous function.')
    argp.add_argument('-T', '--top-level', action='store_true', help='Mangle top-level identifiers.')
    argp.add_argument('-M', '--no-minify', action='store_true', help='Pretty-prints the output.')
    argp.add_argument('-s', '--scrambler', type=str, default='random', help='Scrambler to use.')
    argp.add_argument('--no-double-mangling', action='store_true', help='String-demangler will not, itself, be string-mangled.')

    argp.add_argument('input', type=argparse.FileType('r'), help='Input file.')
    argp.add_argument('output', type=argparse.FileType('w', encoding='utf-8'), help='Output file.')

    args = argp.parse_args()
    js = args.input.read()
    sit = Scramblit(AVAILABLE_SCRAMBLERS[args.scrambler])
    sit.scope.set_external_symbols()
    sit.scope.name_generator = PrefixedNumberNameGenerator()
    js = sit.process_text(js, args.mangle_strings, args.bare, args.mangle, args.mangle and not args.top_level, not args.no_minify, not args.no_double_mangling)
    args.output.write(js)
