'''
BLURB GOES HERE.

Copyright (c) 2017 - 2019 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
# Have to make my own visitor so I can *replace* nodes.

from slimit import ast

class ReplacementVisitor(object):
    NB=0
    def __init__(self):
        self.ignore_attrs=['parent']
        self.parents = {}

    def set_node_parent(self, node, parent, attr):
        nid = str(node)
        self.parents[nid] = (parent, attr)

    def get_node_parent(self, node):
        nid = str(node)
        if nid in self.parents:
            return self.parents[nid]
        else:
            return None

    def visit(self, node, attr=None, parent=None):
        if node:
            self.set_node_parent(node, parent, attr)
        for attr in dir(node):
            if attr in self.ignore_attrs:
                continue
            child = getattr(node, attr, None)
            if isinstance(child, ast.Node):
                ret = self.visit(child, attr, node)
                setattr(node, attr, ret)
            elif isinstance(child, list):
                self.set_node_parent(child, node, attr)
                newchild = []
                for i, grandchild in enumerate(child):
                    gc = self.visit(grandchild, i, child)
                    #print(repr(gc),repr(node))
                    if gc is not None:
                        if hasattr(gc, 'to_ecma') and gc.to_ecma().strip().startswith('{') and gc.to_ecma().strip().endswith('}') and ReplacementVisitor.NB <= 100:
                            ReplacementVisitor.NB+=1
                            #print(repr(node), attr, repr(gc))
                        if isinstance(node, (ast.Block, ast.FuncBase, ast.Case)):
                            #print(repr(node))
                            if isinstance(gc, ast.Block):
                                gc = gc._children_list
                        if isinstance(gc, list):
                            newchild += gc
                        else:
                            newchild += [gc]
                setattr(node, attr, newchild)
        return node
