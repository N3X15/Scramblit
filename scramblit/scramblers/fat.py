'''
BLURB GOES HERE.

Copyright (c) 2017 - 2019 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import random
from scramblit.scramblers.base import Scrambler
from slimit import ast
from typing import List
class FatScrambler(Scrambler):
    def __init__(self, scramblit):
        super().__init__(scramblit)
        self.demangler_func_id = self.scope.assign_function('d',[])
        self._chars=''

    def process_string(self, string:str) -> ast.Node:
        return ast.FunctionCall(self.demangler_func_id.node, [ast.Number(str(self._chars.index(c))) for c in string])

    def addStringToTable(self, string:str) -> ast.Node:
        super().addStringToTable(string)
        return ast.BracketAccessor(self.scramblit.string_table_id.node, self.string_table.index(string))

    def build_runtime(self) -> List[ast.Node]:
        """
        __m=function(){
            w=window;
            S=w.String;
            SFCC=S.fromCharCode;

            var o ='';
            for(var i = 0;i<arguments.length;i++) {
                switch(key[i]) {
                    ...
                    case 'a': o+=SFCC(0); break;
                    ...
                }
            }
            return o;
        };
        """
        scope = self.scope.build_scope()
        id_window = scope.assign_variable('w')
        #id_S = scope.assign_variable('S')
        id_SFCC = scope.assign_variable('S')
        id_i = scope.assign_variable('i')
        id_out = scope.assign_variable('o')
        id_args = scope.assign_variable('a')

        func = ast.FuncDecl(self.demangler_func_id, [], [])
        func.elements.append(ast.VarStatement([
            ast.VarDecl(id_window.node, ast.Identifier('window')),
            ast.VarDecl(
                id_SFCC.node,
                ast.DotAccessor(
                    ast.DotAccessor(
                        id_window.node,
                        ast.Identifier('String')
                    ),
                    ast.Identifier('fromCharCode')
                )
            ),
            ast.VarDecl(id_out.node, ast.String("''")),
            ast.VarDecl(id_args.node, ast.Identifier("arguments")),
        ]))
        # for i, e in enumerate(func.elements):
        #    print(i)
        #    print(e.to_ecma())
        loop = ast.For(
            ast.VarStatement([ast.VarDecl(id_i.node, ast.Number(0))]),
            ast.BinOp('<', id_i.node, ast.DotAccessor(id_args.node, ast.Identifier('length'))),
            ast.UnaryOp('++', id_i.node, False),
            ast.Block([]))
        func.elements.append(loop)

        switch = ast.Switch(
            expr=ast.BracketAccessor(id_args.node, id_i.node),
            cases=[],
            default=None)
        # print(repr(self._chars))
        loop.statement._children_list.append(ast.ExprStatement(switch))

        for cid, c in enumerate(self._chars):
            switch.cases.append(
                ast.Case(
                    expr=ast.Number(cid),
                    elements=[
                        ast.ExprStatement(
                            ast.Assign(
                                op='+=',
                                left=id_out.node,
                                right=ast.FunctionCall(id_SFCC.node, [ast.Number(str(ord(c)))])
                            )
                        ),
                        ast.Break(),
                    ]
                )
            )
        random.shuffle(switch.cases)
        func.elements.append(ast.Return(id_out.node))
        return [func]
