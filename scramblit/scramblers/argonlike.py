'''
Complex string obfuscator based on Argon Stats.

Copyright (c) 2017 - 2019 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
from __future__ import absolute_import

import random
from typing import List

from scramblit.scramblers.base import Scrambler
from slimit import ast


class ArgonLikeScrambler(Scrambler):
    '''
    Based *loosely* on argon.js payload obfuscation.
    '''

    def __init__(self, scramblit):
        super().__init__(scramblit)
        self.demangler_func_id = self.scope.assign_variable('d')

        self.string_table={}

        # Key 1
        self._chars = []
        # Key 2 - Storing this here lets us set the key for testing.
        self.xor_key = random.randint(1, 255)

    def addStringToTable(self, string) -> ast.Node:
        if string not in self.string_table:
            self.string_table[string] = ast.Number(-1)
        return ast.BracketAccessor(self.scramblit.string_table_id.node, self.string_table[string])

    def process_string(self, string: str) -> ast.Node:
        # a('abcdef') -> "hullo or whatever"
        return ast.FunctionCall(self.demangler_func_id.node, [ast.String(repr(''.join([format(self._chars.index(c) ^ self.xor_key, '02X') for c in string])))])

    def build_runtime(self) -> List[ast.Node]:
        return [self.build_descrambler()]

    def getTable(self):
        shuffled = [(k, v) for k, v in self.string_table.items()]
        random.shuffle(shuffled)
        finished_string_table=[]
        self._chars = []
        for i, entry in enumerate(shuffled):
            string, num = entry
            num.value = i
            for char in string:
                if char not in self._chars:
                    self._chars += [char]
            finished_string_table += [string]
        random.shuffle(self._chars)
        for string, number in self.string_table.items():
            number.value = finished_string_table.index(string)
        return finished_string_table

    def build_descrambler(self) -> ast.Node:
        '''
        function a(a){

            var key=[...],
                w=window,
                pi=w.parseInt,
                sfcc=w.String.fromCharCode,
                o='';
            for(var i = 0;i<a.length;i+=2) {
                o += sfcc(key[pi(a[i]+a[i+1], 16)^key[key.length-1]]);
            }
            return o;
        }
        '''
        scope = self.scope.build_scope()
        id_window = scope.assign_variable('W')
        id_S = scope.assign_variable('s')
        id_k1 = scope.assign_variable('k')
        id_parseInt = scope.assign_variable('p')
        id_SFCC = scope.assign_variable('S')
        id_i = scope.assign_variable('i')
        id_out = scope.assign_variable('o')

        func = ast.FuncDecl(self.demangler_func_id.node, [id_S.node], [])
        func.elements.append(ast.VarStatement([
            ast.VarDecl(id_window.node, ast.Identifier('window')),
            ast.VarDecl(
                id_parseInt.node,
                ast.DotAccessor(
                    id_window.node,
                    ast.Identifier('parseInt')
                )
            ),
            ast.VarDecl(
                id_SFCC.node,
                ast.DotAccessor(
                    ast.DotAccessor(
                        id_window.node,
                        ast.Identifier('String')
                    ),
                    ast.Identifier('fromCharCode')
                )
            ),
            ast.VarDecl(id_k1.node, ast.Array([
                ast.Number(str(ord(x) ^ self.xor_key)) for x in self._chars
            ] + [
                ast.Number(str(self.xor_key))
            ])),
            ast.VarDecl(id_out.node, ast.String("''")),
        ]))
        # for i, e in enumerate(func.elements):
        #    print(i)
        #    print(e.to_ecma())
        loop = ast.For(
            ast.VarStatement([ast.VarDecl(id_i.node, ast.Number('0'))]),
            ast.BinOp('<', id_i.node, ast.DotAccessor(id_S.node, ast.Identifier('length'))),
            ast.BinOp('+=', id_i.node, ast.Number('2')),
            ast.Block([]))
        func.elements.append(loop)

        # autopep8 went full ham on this, I am so sorry.
        loop.statement._children_list.append(
            ast.ExprStatement(
                ast.Assign(
                    '+=',
                    id_out.node,
                    ast.FunctionCall(
                        id_SFCC.node,
                        [
                            ast.BinOp('^',
                                      ast.BracketAccessor(
                                          id_k1.node,
                                          ast.BinOp('^',
                                                    ast.FunctionCall(
                                                        id_parseInt.node, [
                                                            ast.BinOp('+',
                                                                      ast.FunctionCall(
                                                                          ast.DotAccessor(
                                                                              id_S.node,
                                                                              ast.Identifier('charAt')
                                                                          ),
                                                                          [id_i.node]
                                                                      ),
                                                                      ast.FunctionCall(
                                                                          ast.DotAccessor(
                                                                              id_S.node,
                                                                              ast.Identifier('charAt')
                                                                          ),
                                                                          [ast.BinOp('+',
                                                                                     id_i.node,
                                                                                     ast.Number('1'))]
                                                                      )
                                                                      ),
                                                            ast.Number('16')
                                                        ]),
                                                    ast.BracketAccessor(id_k1.node,
                                                                        ast.BinOp('-',
                                                                                  ast.DotAccessor(
                                                                                      id_k1.node,
                                                                                      ast.Identifier('length')
                                                                                  ),
                                                                                  ast.Number('1'))
                                                                        )
                                                    )
                                      ),


                                      ast.BracketAccessor(
                                          id_k1.node,
                                          ast.BinOp('-', ast.DotAccessor(
                                              id_k1.node,
                                              ast.Identifier('length')
                                          ),
                                              ast.Number(1))
                                      )
                                      )
                        ])
                )
            ))
        func.elements.append(ast.Return(id_out.node))
        return func
