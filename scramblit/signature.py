'''
Generate a signature that should not be affected by identifier changes.

Copyright (c) 2017 - 2019 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
import math
from buildtools import log

from scramblit.scope.visitor import ScopeVisitor


class SignatureGeneratorVisitor(ScopeVisitor):
    ALPHABET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_$'

    def gen_name_by_sid(self, idn):

        # Basically base-len(alphabet) :V
        base = len(self.ALPHABET)

        # Figure out the length of a string that'll fit a base-whatever number
        # (THANKS STACKOVERFLOW HOLY SHIT.)
        strlen = math.ceil(math.log(idn + 1) / math.log(base))

        buf = [''] * strlen
        i = strlen - 1
        while i > 0 and idn > 0:
            idn /= base
            buf[i] = self.ALPHABET[math.ceil(idn % base)]
            i -= 1
        return ''.join(['_'] + buf)

    def process(self, node):
        tree = super().process(node)
        with log.info('Generating unique names...'):
            for symbol in self.symbols:
                symbol.rename(self.gen_name_by_sid(symbol.ID))
        return tree
