'''
BLURB GOES HERE.

Copyright (c) 2017 - 2019 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
from buildtools import log
from slimit import ast

from scramblit.scope.scope import Scope
from scramblit.visitors import ReplacementVisitor

class ScopeVisitor(ReplacementVisitor):
    def __init__(self):
        super().__init__()
        self._scope = Scope()
        self.root_scope = self.scope
        self.symbols = set()
        self._deferred_until_pop={}
        self.lenient = False
        self.scope.scan_for_decls=False

    def get_scope(self):
        return self._scope

    def set_scope(self, value):
        if not isinstance(value, Scope):
            raise ValueError(value, Scope)
        self._scope=value

    scope = property(get_scope,set_scope)

    def _set_scope_on(self,node,value):
        if not isinstance(value, Scope):
            raise ValueError(value, Scope)
        node.scope=value

    def process(self, node):
        return self.visit(node)

    def push_scope(self, node):
        #print('Pushing scope...')
        self.scope = self.scope.build_scope()
        self.scope.scan_for_decls=False
        if node:
            self._set_scope_on(node, self.scope)

    def pop_scope(self):
        #print('Popping scope {} - {} deferred'.format(self.scope.ID, len(self._deferred_until_pop.get(self.scope.ID,[]))))
        if self.scope.ID in self._deferred_until_pop:
            id_to_pop=self.scope.ID
            for deferred,args in self._deferred_until_pop[id_to_pop]:
                deferred(*args)
            del self._deferred_until_pop[id_to_pop]
        self.scope = self.scope.parent_scope

    def defer_until_pop(self, func, *args):
        if self.scope.ID not in self._deferred_until_pop:
            self._deferred_until_pop[self.scope.ID]=[]
        #print(self.scope.ID, func,args)
        self._deferred_until_pop[self.scope.ID].append((func, args))

    def VarDecl(self,node,attr,parent):
        if self.scope.scan_for_decls:
            return node
        #ast.VarDecl(identifier, initializer)
        #print('VarDecl',node.identifier)
        if node.identifier:
            #print('VarDecl',node.identifier.value)
            symbol = None
            if isinstance(node.initializer, ast.FuncExpr):
                print('Declaring',node.identifier.value+'()')
                symbol = self.scope.assign_function(node.identifier.value, *node.initializer.parameters)
            else:
                print('Declaring',node.identifier.value)
                symbol = self.scope.assign_variable(node.identifier.value)
            node.identifier = symbol.node
            node.initializer = self.visit(node.initializer, 'initializer', node)
            #self._set_scope_on(node, self.scope)
            self._set_scope_on(node.identifier, self.scope)
        return node

    def _process_func_paramblock(self,node):
        for i in range(len(node.parameters)):
            param = node.parameters[i]
            if isinstance(param, ast.Identifier):
                #print('Declaring',param.value)
                node.parameters[i] = self.scope.assign_argument(param.value,i).node
                self._set_scope_on(node.parameters[i], self.scope)
            else:
                log.error('Unidentified TYPE in paramsblock: ',repr(param))
                1/0

    def _process_func_elements(self,_node):
        self.push_scope(_node)
        self.scope.scan_for_decls=True
        if _node.identifier:
            #print(_node.identifier.value)
            _node.symbol = self.scope.assign_function(_node.identifier.value, _node.parameters)
            _node.identifier = _node.symbol.node
            self._set_scope_on(_node.identifier, self.scope)
        self._process_func_paramblock(_node)
        for i in range(len(_node.elements)):
            _node.elements[i] = self.visit(_node.elements[i], i, _node.elements)
        self.scope.scan_for_decls=False
        for i in range(len(_node.elements)):
            _node.elements[i] = self.visit(_node.elements[i], i, _node.elements)
            self._set_scope_on(_node.elements[i], self.scope)
        self.pop_scope()

    def FuncDecl(self,node,attr,parent):
        self._process_func_elements(node)
        self._set_scope_on(node, self.scope)
        #self._set_scope_on(node.identifier, self.scope)
        return node
    def FuncExpr(self,node,attr,parent):
        self._process_func_elements(node)
        self._set_scope_on(node, self.scope)
        #self._set_scope_on(node.identifier, self.scope)
        return node

    def Identifier(self,node,attr,parent):
        assert parent is not None
        assert attr is not None
        self._set_scope_on(node, self.scope)
        if not self.scope.scan_for_decls:
            self.defer_until_pop(self._resolve_id, node, attr, parent)
        return node

    def _get_path(self, node):
        o = [node.__class__.__name__]
        while self.get_node_parent(node) is not None:
            parent, attr = self.get_node_parent(node)
            if isinstance(parent, list):
                o += ['{cls}[{attr}]'.format(attr=str(attr),cls=parent.__class__.__name__)]
            else:
                o += ['{cls}.{attr}'.format(attr=str(attr),cls=parent.__class__.__name__)]
            node = parent
        o.reverse()
        return ' -> '.join(o)

    def _resolve_id(self, node, attr, parent):
        args = -1
        #if isinstance(parent, ast.FunctionCall):
        #    args = len(parent.args)
        print('Resolving',node.value)
        symbol = self.scope.resolve(node.value, args)
        if not symbol:
            log.error('Unidentified identifier: %s in %s', node.value, self._get_path(node))
            #with log.info('Locals:'):
            #    for sig,sym in node.scope.symbols.items():
            #        log.info('%r: %r',sig,sym)
            if not self.lenient:
                raise Exception('Unidentified identifier: {}', node.value)
            if isinstance(parent, (ast.FuncExpr, ast.FuncDecl, ast.FunctionCall)) and attr == 'identifier':
                #print('Lenient: Declaring',node.value+'()')
                symbol = node.scope.assign_function(node.value, parent.args)
            else:
                #print('Lenient: Declaring',node.value)
                symbol = node.scope.assign_variable(node.value)
        if isinstance(parent, list):
            parent[attr]=symbol.node
        else:
            setattr(parent,attr,symbol.node)


    def DotAccessor(self,node,attr,parent):
        #ast.DotAccessor(node, identifier)
        node.node=self.visit(node.node, 'node', node)
        # Do NOT visit identifier.
        self._set_scope_on(node, self.scope)
        self._set_scope_on(node.identifier, self.scope)
        return node

    def visit(self, node, attr=None, parent=None):
        #print(attr, node.__class__.__name__)
        if node:
            self.set_node_parent(node, parent, attr)
        if isinstance(node, ast.VarDecl):
            return self.VarDecl(node,attr,parent)

        elif isinstance(node, ast.FuncDecl):
            return self.FuncDecl(node,attr,parent)

        elif isinstance(node, ast.FuncExpr):
            return self.FuncExpr(node,attr,parent)

        elif isinstance(node, ast.Identifier):
            return self.Identifier(node, attr, parent)

        elif isinstance(node, ast.DotAccessor):
            return self.DotAccessor(node, attr, parent)

        elif isinstance(node, ast.If):
            node.predicate = self.visit(node.predicate, 'predicate', node)

            self.push_scope(node.consequent)
            node.consequent = self.visit(node.consequent, 'consequent', node)
            self.pop_scope()

            self.push_scope(node.alternative)
            node.alternative = self.visit(node.alternative, 'alternative', node)
            self.pop_scope()

        elif isinstance(node, ast.For):
            # ast.For(init, cond, count, statement)
            # for(init; cond; count) statement;
            node.init = self.visit(node.init, 'init', node)
            node.cond = self.visit(node.cond, 'cond', node)
            node.count = self.visit(node.count, 'count', node)
            self.push_scope(node.statement)
            node.statement = self.visit(node.statement, 'statement', node)
            self.pop_scope()

        elif isinstance(node, (ast.Case, ast.Default, ast.Catch, ast.DoWhile, ast.Finally, ast.For, ast.ForIn, ast.While, ast.Program)):
            self.push_scope(node)
            node = super().visit(node, attr, parent)
            self.pop_scope()
        else:
            node = super().visit(node, attr, parent)

        if node:
            self._set_scope_on(node, self.scope)
        return node
