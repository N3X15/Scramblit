'''
Symbols.

Copyright (c) 2017 - 2019 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
from scramblit.scope.enums import NameType
from slimit import ast


class Symbol(object):
    '''
    A symbol within the scope tree.

    Can represent a function (FuncSymbol) or a variable (VarSymbol).

    Attempts to overcome issues with slimit, which I should just fucking recode anyway.
    '''

    def __init__(self, sigtype, name):
        self.name = name
        self.node = ast.Identifier(name)
        self.node.symbol=self
        self.type = sigtype

        self.decl = None
        self.scope = None
        self.refs = []
        self.ID = 0

        self.mangled=False

    def canSetName(self):
        return True

    def become(self, other):
        self.name=other.name
        self.node=other.node
        self.type=other.type
        self.decl=other.decl
        self.scope=other.scope
        self.refs=other.refs
        self.ID=other.ID

    def rename(self, newname):
        oldsig = self.signature
        self.name = newname
        self.node.value = newname
        #print('RENAME',oldsig,self.name)
        if self.scope:
            self.scope._do_rename_symbol(self, oldsig)

    @property
    def signature(self) -> str:
        '''
        Gets the symbol signature.

        Used in hashing.
        '''
        return self.name


class FuncSymbol(Symbol):
    '''
    A symbol for representing a function.
    '''

    def __init__(self, name, args=[]):
        super().__init__(NameType.FUNCTION, name)
        #: We only care about the count, but here for shits and giggles.
        self.args = args

    #def signature(self) -> str:
    #    return f'{self.name}[{len(self.args)}'


class VarSymbol(Symbol):
    '''
    A symbol representing a variable, argument, or constant.
    '''

    def __init__(self, name):
        super().__init__(NameType.VAR, name)

class ExternalSymbol(Symbol):

    def __init__(self, name):
        super().__init__(NameType.VAR, name)

    def rename(self, newname):
        return # lolno

    def canSetName(self):
        return False


class ArgumentSymbol(Symbol):
    '''
    A symbol representing an argument of a function.
    '''

    def __init__(self, name, aid):
        self.Index = aid
        super().__init__(NameType.ARGUMENT, name)
