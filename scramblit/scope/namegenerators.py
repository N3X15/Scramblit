'''
Swappable name generators for mangling ops.

Copyright (c) 2017 - 2019 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''
from math import ceil, log, floor
from typing import Any

from abc import ABC, abstractmethod
from scramblit.scope.enums import NameType
from scramblit.scope.symbols import Symbol

class NameGenerator(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def get_name(self, nametype: NameType, scope: Any) -> str:
        return ''

    @abstractmethod
    def get_name_for(self, symbol:Symbol) -> str:
        return ''


class SequentialASCIINameGenerator(NameGenerator):
    def __init__(self):
        self.alphabet = 'abcdefghijlkmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ'

    def get_name(self, nametype: NameType, scope:Any) -> str:
        idn = len(scope.symbolsByType[nametype])
        return self.gen_name_suffix(idn)

    def get_name_for(self, symbol: Symbol) -> str:
        return self.gen_name_suffix(symbol.ID)

    def gen_name_suffix(self, idn: int):
        if idn == 0:
            return self.alphabet[0]
        # Basically base-len(alphabet) :V
        base = len(self.alphabet)

        # Figure out the length of a string that'll fit a base-whatever number
        # (THANKS STACKOVERFLOW HOLY SHIT.)
        strlen = ceil(log(idn + 1) / log(base))
        #print(strlen)

        buf = [' '] * strlen
        i=strlen-1
        #print(__file__,i,idn,base,strlen,len(buf))
        while i >= 0 and idn >= 0:
            buf[i] = self.alphabet[ceil(idn % base) if idn > 0 else 0]
            idn //= base
            i -= 1
        return ''.join(buf)

class PrefixedNumberNameGenerator(NameGenerator):
    def __init__(self):
        self.symbol_prefixes = {
            NameType.FUNCTION: 'f',
            NameType.VAR:      'v',
            NameType.ARGUMENT: 'a',
        }

    def get_name(self, nametype: NameType, scope:Any) -> str:
        idn = len(scope.symbolsByType[nametype])
        return self.gen_name_suffix(nametype,idn)

    def get_name_for(self, symbol: Symbol) -> str:
        return self.gen_name_suffix(symbol.type, symbol.ID)

    def gen_name_suffix(self, nametype: NameType, idn: int) -> str:
        return '{}{}'.format(self.symbol_prefixes[nametype], idn)
