#!python
import os

from setuptools import find_packages, setup

setup(name='scramblit',
      version='0.0.1',
      description='Javascript Obfuscator',
      author='Rob Nelson',
      author_email='nexisentertainment@gmail.com',
      url='https://www.python.org/sigs/distutils-sig/',
      packages=find_packages(exclude=['*-fixed', 'build']),
      install_requires=[
          'slimit==0.8.1',
      ],
      entry_points={
          'console_scripts': [
              'scramblit=scramblit:main'
          ]
      })
